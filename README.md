# OpenML dataset: hpc-job-scheduling

https://www.openml.org/d/41212

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

HPC Job Scheduling Data as included in the R-package 'AppliedPredictiveModeling' [Max Kuhn and Kjell Johnson (2018). AppliedPredictiveModeling: Functions and Data Sets for 'Applied Predictive Modeling'. R package version 1.1-7. https://CRAN.R-project.org/package=AppliedPredictiveModeling]. To obtain the dataset directly in R, call data(schedulingData, package = 'AppliedPredictiveModeling'). For a description of the dataset checkout the documentation of the R-package. The variable 'Class' was chosen as target variable.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41212) of an [OpenML dataset](https://www.openml.org/d/41212). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41212/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41212/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41212/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

